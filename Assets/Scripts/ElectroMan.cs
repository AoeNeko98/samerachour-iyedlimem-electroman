using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectroMan: MonoBehaviour
{
    public float speed;
    public FloatingJoystick floatingJoystick;
    public float rspeed;
    private Animator anim;
    
    private Rigidbody rb;
    private Vector3 moveDirection;
    private Vector3 moveRotation;
    private Vector3 secondpoint;
    private bool teleportation;


    public void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        rb = gameObject.GetComponent<Rigidbody>();
    }

    public void FixedUpdate()
    {



        moveDirection = new Vector3(0, 0, floatingJoystick.Vertical);
        moveRotation = new Vector3(0, floatingJoystick.Horizontal, 0);
        transform.Translate(moveDirection * speed * Time.fixedDeltaTime);


        transform.Rotate(moveRotation * rspeed * Time.fixedDeltaTime);
        anim.SetFloat("move", floatingJoystick.Vertical);

        /*if (Input.GetTouch(0).phase == TouchPhase.Moved && anim.GetFloat("move") == 0) ;
        {
            secondpoint = Input.GetTouch(0).position;
            transform.Rotate(0,secondpoint.y * 90,0) ;
        }*/
    }

    public void attack()
    {
        anim.SetTrigger("attack");
    }
    public void teleport()
    {
        anim.SetTrigger("teleport");
        Invoke("wait", 2);
        teleportation = true;
    }
    public void specialAttack()
    {
        anim.SetTrigger("attacks");
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Portenter" && teleportation)
        {
            
            transform.position = GameObject.FindGameObjectWithTag("Portexit").transform.position;
            teleportation = false;
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if(collision.gameObject.tag == "Stairs")
        {
            transform.Translate(0, 0.5f, 0);
            Debug.Log("staaaaaa");
        }
    }
    private void wait()
    {

    }
}